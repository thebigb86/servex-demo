<?php

include_once('../vendor/autoload.php');

use App\AppConfiguration;
use Servex\Core\App;

$app = new App(new AppConfiguration());
$app->run();