<?php

namespace App;

use Servex\Core\DefaultConfiguration;

class AppConfiguration extends DefaultConfiguration
{
	public function __construct()
	{
		parent::__construct();
	}
} 